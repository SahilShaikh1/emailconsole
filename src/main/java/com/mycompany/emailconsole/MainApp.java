package com.mycompany.emailconsole;

import javax.mail.PasswordAuthentication;
import java.util.*;
import javax.mail.Authenticator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public class MainApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/Scene.fxml"));

        Scene scene = new Scene(root);
        scene.getStylesheets().add("/styles/Styles.css");

        stage.setTitle("JavaFX and Maven");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {

        String to = "soshu231720@gmail.com";

        String from = "sohailfb20@gmail.com";
        final String username = "Sohail SHAIKH";
        final String password = "arif12345";

        String host = "smtp.gmail.com";

        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", 587);
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", host);
 
       Session session = Session.getInstance(props,
         new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
               return new PasswordAuthentication(
                  "sohailfb20@gmail.com", "feb172023");
            }
         });

        MimeMessage message = new MimeMessage(session);
        session.setDebug(true);

        try {

            message = new MimeMessage(session);

            message.setFrom(new InternetAddress(from));

            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(to));

            message.setSubject("Testing Subject");

            message.setText("Hello, this is sample for to check send "
                    + "email using JavaMailAPI ");

            Transport.send(message);

            System.out.println("Sent message successfully....");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}
